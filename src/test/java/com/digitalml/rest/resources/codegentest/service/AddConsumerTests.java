package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.ConsumerService.ConsumerServiceServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ConsumerServiceService.AddConsumerInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ConsumerServiceService.AddConsumerReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class AddConsumerTests {

	@Test
	public void testOperationAddConsumerBasicMapping()  {
		ConsumerServiceServiceDefaultImpl serviceDefaultImpl = new ConsumerServiceServiceDefaultImpl();
		AddConsumerInputParametersDTO inputs = new AddConsumerInputParametersDTO();
		inputs.setRequest(org.apache.commons.lang3.StringUtils.EMPTY);
		AddConsumerReturnDTO returnValue = serviceDefaultImpl.addConsumer(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}